<?php
/**
 * The functions file is used to initialize everything in the theme.  It controls how the theme is loaded and 
 * sets up the supported features, default actions, and default filters.  If making customizations, users 
 * should create a child theme and make changes to its functions.php file (not this one).  Friends don't let 
 * friends modify parent theme files. ;)
 *
 * Child themes should do their setup on the 'after_setup_theme' hook with a priority of 11 if they want to
 * override parent theme features.  Use a priority of 9 if wanting to run before the parent theme.
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write 
 * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * @package Plain
 * @subpackage Functions
 * @version 0.1.0
 * @author Justin Tadlock <justin@justintadlock.com>
 * Copyright (c) 2013, Justin Tadlock
 * @link http://themehall.com/preview/plain
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* Load the core theme framework. */
require_once( trailingslashit( get_template_directory() ) . 'library/hybrid.php' );
new Hybrid();

/* Do theme setup on the 'after_setup_theme' hook. */
add_action( 'after_setup_theme', 'plain_theme_setup' );

/**
 * Theme setup function.  This function adds support for theme features and defines the default theme
 * actions and filters.
 *
 * @since  0.1.0
 * @access public
 * @return void
 */
function plain_theme_setup() {

	/* Get action/filter hook prefix. */
	$prefix = hybrid_get_prefix();

	/* Register menus. */
	add_theme_support( 
		'hybrid-core-menus', 
		array( 'primary') 
	);

	/* Register sidebars. */
	add_theme_support( 
		'hybrid-core-sidebars', 
		array( 'primary', 'subsidiary' ) 
	);

	/* Load scripts. */
	add_theme_support( 
		'hybrid-core-scripts', 
		array( 'comment-reply' ) 
	);

	/* Load styles. */
	add_theme_support( 
		'hybrid-core-styles', 
		array( 'parent', 'style' ) 
	);

	/* Load widgets. 
	add_theme_support( 'hybrid-core-widgets' ); // hence disabled to pass themecheck
	*/
	
	/* Load shortcodes. */
	add_theme_support( 'hybrid-core-shortcodes' );
	
	add_theme_support( 'hybrid-core-theme-settings', array( 'about', 'footer' ) );
	
	/* Enable custom template hierarchy. */
	add_theme_support( 'hybrid-core-template-hierarchy' );

	/* Enable theme layouts (need to add stylesheet support). */
	add_theme_support( 
		'theme-layouts', 
		array( '1c', '2c-l', '2c-r' ), 
		array( 'default' => '2c-l', 'customizer' => true ) 
	);

	/* Allow per-post stylesheets. */
	add_theme_support( 'post-stylesheets' );
	
	add_editor_style();

	/* Support pagination instead of prev/next links. */
	add_theme_support( 'loop-pagination' );

	/* The best thumbnail/image script ever. */
	add_theme_support( 'get-the-image' );

	/* Use breadcrumbs. */
	add_theme_support( 'breadcrumb-trail' );

	/* Nicer [gallery] shortcode implementation. */
	// add_theme_support( 'cleaner-gallery' ); disable to support jetpack Tiled Galleries

	/* Better captions for themes to style. */
	add_theme_support( 'cleaner-caption' );

	/* Automatically add feed links to <head>. */
	add_theme_support( 'automatic-feed-links' );

	/* Post formats. */
	add_theme_support( 
		'post-formats', 
		array( 'aside', 'audio', 'chat', 'image', 'gallery', 'link', 'quote', 'status', 'video' ) 
	);

	/* Add support for a custom header image. */
	add_theme_support(
		'custom-header',
		array( 'header-text' => false,
			'default-image' => get_template_directory_uri() . '/images/header.jpg' ) );

	/* Custom background. */
	add_theme_support( 
		'custom-background',
		array( 'default-color' => 'f5f5f5' )
	);

	/* Handle content width for embeds and images. */
	hybrid_set_content_width( 1040 );

	/* Enqueue scripts (and related stylesheets) */
	add_action( 'wp_enqueue_scripts', 'plain_scripts' );
	
	/* Additional and conditional styles */
	add_action( 'wp_head', 'plain_styles' );
	
	/* Wrap embeds with some custom HTML to handle responsive layout. */
	add_filter( 'embed_handler_html', 'plain_embed_html' );
	add_filter( 'embed_oembed_html',  'plain_embed_html' );

	/* Filter the sidebar widgets. */
	add_filter( 'sidebars_widgets', 'plain_disable_sidebars' );
	add_action( 'template_redirect', 'plain_one_column' );

	/** Hybrid Core 1.6 changes **/
	add_filter( "{$prefix}_sidebar_defaults", 'plain_sidebar_defaults' );
	add_filter( 'cleaner_gallery_defaults',   'plain_gallery_defaults' );
	add_filter( 'the_content', 'plain_aside_infinity', 9 );
	/****************************/
	
	/* Filter size of the gravatar on comments. */
	add_filter( "{$prefix}_list_comments_args", 'plain_comments_args' );
	
	/** custom excerpt **/
	add_filter('excerpt_more', 'plain_excerpt_more');
}

/* === HYBRID CORE 1.6 CHANGES. === 
 *
 * The following changes are slated for Hybrid Core version 1.6 to make it easier for 
 * theme developers to build awesome HTML5 themes. The code will be removed once 1.6 
 * is released.
 */

	/**
	 * Content template.  This is an early version of what a content template function will look like
	 * in future versions of Hybrid Core.
	 *
	 * @since  0.1.0
	 * @access public
	 * @return void
	 */
	function plain_get_content_template() {

		$templates = array();
		$post_type = get_post_type();

		if ( post_type_supports( $post_type, 'post-formats' ) ) {

			$post_format = get_post_format() ? get_post_format() : 'standard';

			$templates[] = "content-{$post_type}-{$post_format}.php";
			$templates[] = "content-{$post_format}.php";
		}

		$templates[] = "content-{$post_type}.php";
		$templates[] = 'content.php';

		return locate_template( $templates, true, false );
	}

	/**
	 * Sidebar parameter defaults.
	 *
	 * @since  0.1.0
	 * @access public
	 * @param  array  $defaults
	 * @return array
	 */
	function plain_sidebar_defaults( $defaults ) {

		$defaults = array(
			'before_widget' => '<section id="%1$s" class="widget %2$s widget-%2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>'
		);

		return $defaults;
	}

	/**
	 * Gallery defaults for the Cleaner Gallery extension.
	 *
	 * @since  0.1.0
	 * @access public
	 * @param  array  $defaults
	 * @return array
	 */
	function plain_gallery_defaults( $defaults ) {

		$defaults['itemtag']    = 'figure';
		$defaults['icontag']    = 'div';
		$defaults['captiontag'] = 'figcaption';

		return $defaults;
	}

	/**
	 * Adds an infinity character "&#8734;" to the end of the post content on 'aside' posts.  This 
	 * is from version 0.1.1 of the Post Format Tools extension.
	 *
	 * @since  0.1.0
	 * @access public
	 * @param  string $content The post content.
	 * @return string $content
	 */
	function plain_aside_infinity( $content ) {

		if ( has_post_format( 'aside' ) && !is_singular() )
			$content .= ' <a class="permalink" href="' . get_permalink() . '" title="' . the_title_attribute( array( 'echo' => false ) ) . '">&#8734;</a>';

		return $content;
	}

/* End Hybrid Core 1.6 section. */

/**
 * Registers scripts for the theme and enqueue those used sitewide.
 *
 * @since 0.1.0.
 */

function plain_scripts() {	
	wp_enqueue_script( 'navigation', get_template_directory_uri() . '/js/navigation.js', array( 'jquery' ));
	wp_enqueue_style('montserrat', 'http://fonts.googleapis.com/css?family=Montserrat:400,700');
}

/**
 * Insert conditional styles for the theme used sitewide.
 *
 * @since 0.1.0.
 */
function plain_styles() {
?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/desktop.css' type='text/css' media='all' />
<![endif]-->
<?php 
}

/**
 * Function for deciding which pages should have a one-column layout.
 *
 * @since  0.1.0
 * @access public
 * @return void
 */
function plain_one_column() {

	if ( !is_active_sidebar( 'primary' ) )
		add_filter( 'theme_mod_theme_layout', 'plain_theme_layout_one_column' );

	elseif ( is_attachment() && wp_attachment_is_image() && 'default' == get_post_layout( get_queried_object_id() ) )
		add_filter( 'theme_mod_theme_layout', 'plain_theme_layout_one_column' );

}

/**
 * Filters 'get_theme_layout' by returning 'layout-1c'.
 *
 * @since  0.1.0
 * @param  string $layout The layout of the current page.
 * @return string
 */
function plain_theme_layout_one_column( $layout ) {
	return '1c';
}


/**
 * Disables sidebars if viewing a one-column page.
 *
 * @since  0.1.0
 * @param  array $sidebars_widgets A multidimensional array of sidebars and widgets.
 * @return array $sidebars_widgets
 */

function plain_disable_sidebars( $sidebars_widgets ) {
	global $wp_customize;

	$customize = ( is_object( $wp_customize ) && $wp_customize->is_preview() ) ? true : false;

	if ( !is_admin() && !$customize && '1c' == get_theme_mod( 'theme_layout' ) )
		$sidebars_widgets['primary'] = false;

	return $sidebars_widgets;
}

/**
 * Wraps embeds with <div class="embed-wrap"> to help in making videos responsive.
 *
 * @since  0.1.0
 * @access public
 * @return void
 * @author Justin Tadlock <justin@justintadlock.com>
 */
function plain_embed_html( $html ) {

	if ( in_the_loop() && has_post_format( 'video' ) && preg_match( '/(<embed|object|iframe)/', $html ) )
		$html = '<div class="embed-wrap">' . $html . '</div>';

	return $html;
}

/**
 * Filter size of the gravatar on comments.
 * 
 * @since 0.1.0
 */
function plain_comments_args( $args ) {
	$args['avatar_size'] = 48;
	return $args;
}

/**
 * Replaces the excerpt "more" text by a link
 *
 * @since 0.1.0
 */
function plain_excerpt_more($more) {
    global $post;
	return ' ... <a class="moretag" href="'. get_permalink($post->ID) . '">'.__( 'more', 'plain' ).'</a>';
}


?>