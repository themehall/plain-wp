	<article id="post-0" class="<?php hybrid_entry_class(); ?>">

		<header class="entry-header">
			<h1 class="entry-title"><?php _e( 'Nothing found', 'plain' ); ?></h1>
		</header>

		<div class="entry-content">
			<p><?php _e( 'Apologies, but no entries were found.', 'plain' ); ?></p>
		</div><!-- .entry-content -->

	</article><!-- .hentry .error -->