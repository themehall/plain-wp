Plain WordPress theme
---------------------
Plain is a minimalist responsive, mobile first parent theme for WordPress

Features
--------
Plain is built from the famous Hybrid Core theme framework, so it provides many useful features: Loop Pagination, Theme Layouts, Translation-friendly, and more

Credits
- This theme makes use of Justin Tadlock's Hybrid Base theme
- This theme was adopted from James Geiger's Seamless theme
- Header image by Alejandro Escamilla and carry the license CC Zero, http://creativecommons.org/choose/zero/

License
-------
plain is licensed under the GNU General Public License, version 2 or later (GPL).